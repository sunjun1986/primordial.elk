﻿#if NOGO
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Microsoft.Build.Framework;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Primordial.ELK.MSBuild
{
    public class LogstashNodeLogger : INodeLogger
    {
        private static readonly char[] FileLoggerParameterDelimiters = new[] {';'};
        private static readonly char[] FileLoggerParameterValueSplitCharacter = new[] {'='};

// ReSharper disable NotAccessedField.Local
        private int _numberOfProcessors;
// ReSharper restore NotAccessedField.Local

        //private Encoding _encoding;
        private string _type;
        private string _redisHost;
        private int _redisPort;
        private RedisClient _redisClient;

        public LoggerVerbosity Verbosity { get; set; }
        public string Parameters { get; set; }

        public LogstashNodeLogger()
        {
            //_encoding = Encoding.Default;
            _type = typeof (LogstashNodeLogger).Name;
            _redisHost = "localhost";
            _redisPort = RedisClient.DefaultPort;
        }

        public void Initialize(IEventSource eventSource, int nodeCount)
        {
            this.InitializeFileLogger(eventSource, nodeCount);
        }

        public void Initialize(IEventSource eventSource)
        {
            this.InitializeFileLogger(eventSource, 1);
        }

        private void InitializeFileLogger(IEventSource eventSource, int nodeCount)
        {
            _numberOfProcessors = nodeCount;

            if (eventSource == null) return;

            //string parameters = base.Parameters;
            //if (parameters != null)
            //{
            //    base.Parameters = "FORCENOALIGN;" + parameters;
            //}
            //else
            //{
            //    base.Parameters = "FORCENOALIGN;";
            //}
            this.ParseFileLoggerParameters();
            //base.Initialize(eventSource, nodeCount);

            Debug.Print("Create RedisClient {0}:{1}", _redisHost, _redisPort);
            if (_redisClient != null) throw new InvalidOperationException("Redis client already initialized.");
            _redisClient = new RedisClient(_redisHost, _redisPort);

            eventSource.BuildStarted += BuildStartedHandler;
            eventSource.BuildFinished += BuildFinishedHandler;
            //eventSource.ProjectStarted += ProjectStartedHandler;
            //eventSource.ProjectFinished += ProjectFinishedHandler;
            //eventSource.TargetStarted += TargetStartedHandler;
            //eventSource.TargetFinished += TargetFinishedHandler;
            //eventSource.TaskStarted += TaskStartedHandler;
            //eventSource.TaskFinished += TaskFinishedHandler;
            eventSource.ErrorRaised += ErrorHandler;
            eventSource.WarningRaised += WarningHandler;
            eventSource.MessageRaised += MessageHandler;
            //eventSource.CustomEventRaised += CustomEventHandler;




            //try
            //{
            //    this.fileWriter = new StreamWriter(this.logFileName, this.append, this.encoding);
            //    this.fileWriter.AutoFlush = true;
            //}
            //catch (Exception exception)
            //{
            //    string str2;
            //    string str3;
            //    if (ExceptionHandling.NotExpectedException(exception))
            //    {
            //        throw;
            //    }
            //    string message = ResourceUtilities.FormatResourceString(out str2, out str3, "InvalidFileLoggerFile", new object[] { this.logFileName, exception.Message });
            //    if (this.fileWriter != null)
            //    {
            //        this.fileWriter.Close();
            //    }
            //    throw new LoggerException(message, exception.InnerException, str2, str3);
            //}
        }

        public void Shutdown()
        {
            Debug.WriteLine("LogstashNodeLogger.Shutdown()");
            if (_redisClient != null)
            {
                _redisClient.QUIT();
                _redisClient.Dispose();
                _redisClient = null;
            }
        }

        //private RedisClient GetRedisClient()
        //{
        //    if (_redisClient == null)
        //    {
        //        Debug.Print("Create RedisClient {0}:{1}", _redisHost, _redisPort);
        //        _redisClient = new RedisClient(_redisHost, _redisPort);
        //    }
        //    return _redisClient;
        //}

        private void ParseFileLoggerParameters()
        {
            if (this.Parameters != null)
            {
                string[] strArray = this.Parameters.Split(FileLoggerParameterDelimiters);
                for (int i = 0; i < strArray.Length; i++)
                {
                    if (strArray[i].Length > 0)
                    {
                        string[] strArray2 = strArray[i].Split(FileLoggerParameterValueSplitCharacter);
                        if (strArray2.Length > 1)
                        {
                            this.ApplyFileLoggerParameter(strArray2[0], strArray2[1]);
                        }
                        else
                        {
                            this.ApplyFileLoggerParameter(strArray2[0], null);
                        }
                    }
                }
            }
        }

        private void ApplyFileLoggerParameter(string parameterName, string parameterValue)
        {
            var str = parameterName.ToUpperInvariant();
            if (string.IsNullOrEmpty(str)) return;
            switch (str)
            {
                case "HOST":
                    _redisHost = parameterValue;
                    return;
                case "PORT":
                    if (!int.TryParse(parameterValue, out _redisPort)) _redisPort = RedisClient.DefaultPort;
                    return;
                case "TYPE":
                    _type = parameterValue;
                    return;
            }
        }

        // --------------------------------------------

        private void BuildStartedHandler(object sender, BuildStartedEventArgs e)
        {
            //Debug.WriteLine("LogstashNodeLogger.BuildStartedHandler()");
            LogToRedis(CreateJson(e));
        }

        private void BuildFinishedHandler(object sender, BuildFinishedEventArgs e)
        {
            //Debug.WriteLine("LogstashNodeLogger.BuildFinishedHandler()");
            LogToRedis(CreateJson(e));
        }

        private void MessageHandler(object sender, BuildMessageEventArgs e)
        {
            //Debug.WriteLine("LogstashNodeLogger.MessageHandler()");
            LogToRedis(CreateJson(e));
        }

        private void LogToRedis(string json)
        {
            //var redisClient = GetRedisClient();
            //Debug.Print("LogToRedis '{0}' '{1}'", _redisClient, json);
            //if (redisClient == null) throw new NullReferenceException("Unable to get redis client.");
            if (_redisClient != null) _redisClient.SendJsonToRedis(json);
        }

        private void ProjectStartedHandler(object sender, ProjectStartedEventArgs e)
        {
            WriteLog("ProjectStarted", "{0} :: {1}", e.Timestamp, e.Message);
        }

        private void ProjectFinishedHandler(object sender, ProjectFinishedEventArgs e)
        {
            WriteLog("ProjectFinished", "{0} :: {1}", e.Timestamp, e.Message);
        }

        private void TargetStartedHandler(object sender, TargetStartedEventArgs e)
        {
            WriteLog("TargetStarted", "{0} :: {1}", e.Timestamp, e.Message);
        }

        private void TargetFinishedHandler(object sender, TargetFinishedEventArgs e)
        {
            WriteLog("TargetFinished", "{0} :: {1}", e.Timestamp, e.Message);
        }

        private void TaskStartedHandler(object sender, TaskStartedEventArgs e)
        {
            WriteLog("TaskStarted", "{0} :: {1}", e.Timestamp, e.Message);
        }

        private void TaskFinishedHandler(object sender, TaskFinishedEventArgs e)
        {
            WriteLog("TaskFinished", "{0} :: {1}", e.Timestamp, e.Message);
        }

        private void ErrorHandler(object sender, BuildErrorEventArgs e)
        {
            WriteLog("ErrorRaised", "{0} :: {1}", e.Timestamp, e.Message);
        }

        private void WarningHandler(object sender, BuildWarningEventArgs e)
        {
            WriteLog("WarningRaised", "{0} :: {1}", e.Timestamp, e.Message);
        }

        private void CustomEventHandler(object sender, CustomBuildEventArgs e)
        {
            WriteLog("CustomEventRaised", "{0} :: {1}", e.Timestamp, e.Message);
        }

        private bool IsVerbosityAtLeast(LoggerVerbosity checkVerbosity)
        {
            return (Verbosity >= checkVerbosity);
        }

        private void WriteLog(string action, string format, params object[] arg)
        {
            Console.Write("{0,-20} :: ", action);
            Console.WriteLine(format, arg);
        }

        //private void WriteLog(string format, params object[] arg)
        //{
        //    Console.WriteLine(format, arg);
        //}

        //private void WriteLog(string text)
        //{
        //    Console.WriteLine(text);
        //}

        private string CreateJson(DateTime timestamp, string message, MessageImportance importance, params string[] extraTags)
        {
            //var message = Layout.Render(logEvent);

            var tags = new List<string>();
            tags.Add("msbuild");
            tags.Add(importance.ToString());
            if (extraTags != null) tags.AddRange(extraTags);

            var fields = new Dictionary<string, object>();
            fields.Add("importance", importance);

            return CreateJson(
                "msbuild",
                message,
                timestamp,
                _type,
                tags,
                fields);
        }

        private string CreateJson(BuildMessageEventArgs logEvent)
        {
            return CreateJson(logEvent.Timestamp, logEvent.Message, logEvent.Importance);
        }

        private string CreateJson(BuildStartedEventArgs logEvent)
        {
            return CreateJson(logEvent.Timestamp, logEvent.Message, MessageImportance.High, "build-started");
        }

        private string CreateJson(BuildFinishedEventArgs logEvent)
        {
            return CreateJson(logEvent.Timestamp, logEvent.Message, MessageImportance.High, "build-finished");
        }


        public static string CreateJson(
            string source, string message,
            DateTime timeStamp, string type,
            IEnumerable<object> tags,
            IEnumerable<KeyValuePair<string, object>> fields)
        {
            var logEvent = new JObject();
            logEvent.AddMantory(source, timeStamp);
            //logEvent["@type"] = type;
            if (tags != null)
            {
                var tagArray = tags.ToArray();
                if (tagArray.Length > 0)
                {
                    logEvent.Add("@tags", new JArray(tagArray));
                }
            }
            if (fields != null)
            {
                var fieldList = new JObject();
                foreach (var fld in fields)
                {
                    fieldList.Add(fld.Key, new JValue(fld.Value));
                }
                logEvent.Add("@fields", fieldList);
            }
            logEvent["@message"] = message;
            return JsonConvert.SerializeObject(logEvent);
        }
    }
}
#endif