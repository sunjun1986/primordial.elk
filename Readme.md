# Primordial.ELK

The ELK stack is ElasticSearch, Logstash and Kibana.

[ELK Stack Downloads](https://www.elastic.co/downloads)

## Primordial.ELK

[![NuGet](https://img.shields.io/nuget/v/primordial.elk.svg?maxAge=86400)](https://www.nuget.org/packages/Primordial.ELK/)

ElasticSearch, Logstash and Kibana

## Primordial.ELK.NLog

[![NuGet](https://img.shields.io/nuget/v/primordial.elk.nlog.svg?maxAge=86400)](https://www.nuget.org/packages/Primordial.ELK.NLog/)

ELK for NLog

## Primordial.ELK.log4net

[![NuGet](https://img.shields.io/nuget/v/primordial.elk.log4net.svg?maxAge=86400)](https://www.nuget.org/packages/Primordial.ELK.log4net/)

ELK for log4net

## Primordial.ELK.MSBuild

ELK for MSBuild
