﻿using System.Collections;
using System.IO;
using System.Text;
using log4net.Core;
using log4net.Layout;
using Primordial.ELK.Kibana;

namespace Primordial.ELK.log4net
{
    public class KibanaLayout : PatternLayout
    {
        public KibanaLayout()
            : this("%message%newline")
        {
        }

        public KibanaLayout(string pattern)
            : base(pattern)
        {
            IgnoresException = false;
        }

        public override void Format(TextWriter writer, LoggingEvent loggingEvent)
        {
            //var locationInformation = loggingEvent.LocationInformation;
            var messageEvent = new LogEvent();

            messageEvent.timestamp = loggingEvent.TimeStamp;
            messageEvent.AddLogInfo(loggingEvent.Level.ToString(), loggingEvent.LoggerName, "log4net");

            //messageEvent.AddTags("E247.Logstash", "log4net", loggingEvent.Level.ToString());

            messageEvent.AddException(loggingEvent.ExceptionObject);

            var properties = loggingEvent.GetProperties();
            foreach (DictionaryEntry property in properties)
            {
                var key = property.Key.ToString();
                if (key.Contains(":")) continue;
                messageEvent.AddField(key, property.Value.ToString());
            }

            var sb = new StringBuilder();
            using (var sw = new StringWriter(sb))
            {
                base.Format(sw, loggingEvent);
            }
            messageEvent.message = sb.ToString();

            var json = messageEvent.ToJson();
            writer.WriteLine(json);

#if NOGO
            var fields = new JObject();
            fields.Add("application", new JObject
            {
#if FORMSAPP
                {"name", Application.ProductName},
                {"version", Application.ProductVersion},
                {"executable", Application.ExecutablePath},
#endif
                {"basepath", AppDomain.CurrentDomain.BaseDirectory},
                {"domain", loggingEvent.Domain},
                {"uptime", (loggingEvent.TimeStamp - LoggingEvent.StartTime)}
            });
            fields.Add("host", new JObject
            {
                {"os_version", Globals.OsVersion},
                {"ips", Globals.HostIps}
            });
            fields.Add("log", new JObject
            {
                {"level", loggingEvent.Level.ToString()},
                {"name", loggingEvent.LoggerName}
            });
            //fields.Add("threadName",  loggingEvent.ThreadName );

            if (locationInformation != null)
            {
                //fields.Add("location_info", locationInformation.FullInfo);
                fields.Add("location", new JObject
                {
                    { "path", locationInformation.FileName },
                    { "line", locationInformation.LineNumber },
                    { "class", locationInformation.ClassName },
                    { "method", locationInformation.MethodName }
                });
            }

            fields.Add("user_name", loggingEvent.UserName);
            if (!string.IsNullOrWhiteSpace(loggingEvent.Identity))
            {
                fields.Add("identity", loggingEvent.Identity);
            }

            //fields.Add("configFile", appDomain.SetupInformation.ConfigurationFile);

            var ex = loggingEvent.ExceptionObject;
            if (ex != null)
            {
                fields.Add("exception", new JObject
                {
                    {"message", ex.Message},
                    {"stacktrace", ex.StackTrace}
                });
            }

            var properties = loggingEvent.GetProperties();
            foreach (DictionaryEntry property in properties)
            {
                var key = property.Key.ToString();
                if (key.Contains(":")) continue;
                fields.Add(key, property.Value.ToString());
            }

            jEvent.Add("@fields", fields);

            var sb = new StringBuilder();
            using (var sw = new StringWriter(sb))
            {
                base.Format(sw, loggingEvent);
            }
            jEvent["@message"] = sb.ToString();

            var json = JsonConvert.SerializeObject(jEvent, Formatting.Indented);
            //Debug.WriteLine("json-layout=" + json);
#endif
        }
    }
}