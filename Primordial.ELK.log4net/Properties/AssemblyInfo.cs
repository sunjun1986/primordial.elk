using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Primordial.ELK.log4net")]
[assembly: AssemblyDescription("ELK for log4net 1.2.10")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Hyrtwol")]
[assembly: AssemblyProduct("Primordial.ELK.log4net")]
[assembly: AssemblyCopyright("Copyright Hyrtwol 2015")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]
[assembly: Guid("da95f6b8-444c-4574-8356-0ba427eb1d7f")]

[assembly: AssemblyVersion("1.0.1")]
[assembly: AssemblyFileVersion("1.0.1")]
