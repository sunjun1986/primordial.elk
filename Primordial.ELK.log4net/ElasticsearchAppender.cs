﻿using System;
using System.Diagnostics;
using log4net.Appender;
using log4net.Core;
using Primordial.ELK.Elasticsearch;

namespace Primordial.ELK.log4net
{
    public class ElasticsearchAppender : AppenderSkeleton
    {
        public string ElasticsearchUrl { get; set; }

        public string Type { get; set; }

        public ElasticsearchAppender()
        {
            Type = typeof(ElasticsearchAppender).Name;
        }
        
        protected override void OnClose()
        {
            Debug.WriteLine("ElasticsearchAppender.OnClose()");
            base.OnClose();
        }

        protected override void Append(LoggingEvent loggingEvent)
        {
            if (loggingEvent == null) throw new ArgumentNullException(nameof(loggingEvent));
            var json = this.RenderLoggingEvent(loggingEvent);

            var elasticsearchUrl = new Uri(ElasticsearchUrl);
            ElasticsearchUtils.SendToElasticsearch(elasticsearchUrl, json);
        }
    }
}
