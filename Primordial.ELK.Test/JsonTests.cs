﻿using System;
using System.Diagnostics;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using NUnit.Framework;
using Primordial.ELK.Kibana;

namespace Primordial.ELK.Test
{
    [TestFixture]
    public class JsonTests
    {
        [Test]
        public void SerializeMessageEvent()
        {
            //var settings = new JsonSerializerSettings();
            //settings.Formatting = Formatting.Indented;
            //settings.ContractResolver = new KibanaPropertyNamesContractResolver();

            var messageEvent = new LogEvent
            {
                timestamp = DateTime.Now,
                message = "TEXT MESSAGE"
            };

            messageEvent.AddLogInfo("Error", "logger-name", "NLog");
            messageEvent.AddException(new NotSupportedException("Not a real exception! 1"));
            messageEvent.AddField("rip", "one");
            messageEvent.AddField("rap", "two");
            messageEvent.AddField("rup", "three");
            messageEvent.AddTags("unittest");

            var json = messageEvent.ToJson();
            Debug.WriteLine(json);
        }

        [Test]
        public void SerializeJObject()
        {
            var settings = new JsonSerializerSettings();
            settings.Formatting = Formatting.Indented;
            settings.ContractResolver = new FalseContractResolver();
            var jEvent = new JObject();
            jEvent["@timestamp"] = DateTime.Now.ToString("o");
            jEvent["os_version"] = Globals.OsVersion;
            jEvent["Test"] = "dfdf";
            var json = JsonConvert.SerializeObject(jEvent, settings);
            Debug.WriteLine(json);
        }

        [Test]
        public void SerializeObjectIndented()
        {
            var settings = new JsonSerializerSettings();
            settings.Formatting = Formatting.Indented;
            settings.ContractResolver = new FalseContractResolver();
            var json = SerializeWithSettings(settings);
            Debug.WriteLine(json);
        }

        [Test]
        public void CamelCasePropertyNamesContractResolverTest()
        {
            var settings = new JsonSerializerSettings();
            settings.Formatting = Formatting.Indented;
            settings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            var json = SerializeWithSettings(settings);
            Debug.WriteLine(json);
        }

        [Test]
        public void KibanaPropertyNamesContractResolverTest()
        {
            var settings = new JsonSerializerSettings();
            settings.Formatting = Formatting.Indented;
            settings.ContractResolver = new KibanaPropertyNamesContractResolver();
            var json = SerializeWithSettings(settings);
            Debug.WriteLine(json);
        }

        private static string SerializeWithSettings(JsonSerializerSettings settings)
        {
            object obj = new
            {
                @timestamp = DateTime.Now,
                os_version = Globals.OsVersion,
                Test = "dfdf",
                fields = new
                {
                    justATest = 666
                }
            };
            return JsonConvert.SerializeObject(obj, settings);
        }

        public class FalseContractResolver : DefaultContractResolver
        {
            public FalseContractResolver()
            {
            }

            protected override string ResolvePropertyName(string propertyName)
            {
                return "_FAKE_"+ propertyName;
            }
        }
    }
}
