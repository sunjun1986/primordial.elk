﻿using System;
using System.Diagnostics;
using NUnit.Framework;
using Primordial.ELK.Elasticsearch;

namespace Primordial.ELK.Test
{
    [TestFixture]
    public class ElasticsearchUtilsTests
    {
        [Test]
        public void ElasticsearcIndexUrl()
        {
            var baseUri = new Uri("http://localhost:9200");
            Debug.Print("baseUri  = {0}", baseUri);

            Assert.AreEqual("http://localhost:9200/", baseUri.ToString());

            var indexUri = ElasticsearchUtils.CreateElasticsearcIndexUrl(baseUri);
            Debug.Print("indexUri = {0}", indexUri);

            indexUri = ElasticsearchUtils.CreateElasticsearcIndexUrl(
                baseUri, "someindex", "sometype", "someid");
            Debug.Print("indexUri = {0}", indexUri);

            Assert.AreEqual("http://localhost:9200/someindex/sometype/someid", indexUri.ToString());
        }
    }
}