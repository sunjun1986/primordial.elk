﻿using System;
using System.Diagnostics;
using NLog;
using NLog.Config;
using NLog.Targets;
using NUnit.Framework;
using Primordial.ELK.Test;

namespace Primordial.ELK.NLog.Test
{
#if IGNORE_UNIT_TEST
    [Ignore("TeamCity is unable to connect to the remote host")]
#endif
    [TestFixture]
    public class ElasticsearchTargetTests
    {
        [Test]
        public void LogLevels()
        {
            Configure(@"logstash-nlog.log");

            var logger = LogManager.GetLogger("Test");
            Assert.IsNotNull(logger);
            logger.Debug("Fake Debug");
            logger.Info("Fake Info");
            logger.Warn("Fake Warn");
            logger.Error("Fake Error");
            logger.Fatal("Fake Fatal");
        }

        [Test]
        public void LogError()
        {
            Configure(@"logstash-nlog.log");

            var logger = LogManager.GetLogger("Test");
            Assert.IsNotNull(logger);

            for (int i = 0; i < 3; i++)
            {
                //Debug.WriteLine("Logging warning: {0}", i);
                //logger.ErrorFormat("Fake Error {0}", i);
                //logger.Error("Fake Error " + i, new Excep);

                try
                {
                    throw new NotSupportedException("Not a real exception! " + i);
                }
                catch (Exception ex)
                {
                    //Debug.WriteLine("Logging error");
                    logger.Error(ex, "Fake error " + i);
                }
            }
        }

        [Test]
        public void LogErrorFileConfig()
        {
            //Configure(@"logstash-nlog.log");
            LogManager.Configuration = new XmlLoggingConfiguration("test.config");

            foreach (var target in LogManager.Configuration.AllTargets)
            {
                Debug.Print("target={0}", target);
            }

            foreach (var loggingRule in LogManager.Configuration.LoggingRules)
            {
                Debug.Print("loggingRule={0}", loggingRule);
            }

            var logger = LogManager.GetLogger("Test");
            Assert.IsNotNull(logger);

            for (int i = 0; i < 3; i++)
            {
                //Debug.WriteLine("Logging warning: {0}", i);
                //logger.ErrorFormat("Fake Error {0}", i);
                //logger.Error("Fake Error " + i, new Excep);

                try
                {
                    throw new NotSupportedException("Not a real exception! " + i);
                }
                catch (Exception ex)
                {
                    //Debug.WriteLine("Logging error");
                    logger.Error(ex, "Fake error " + i);
                }
            }
        }

        public static void Configure(string logFileName)
        {
            //var ff = new AspNetBufferingTargetWrapper();

            // Step 1. Create configuration object 
            var config = new LoggingConfiguration();

            // Step 2. Create targets and add them to the configuration 
            var consoleTarget = new ConsoleTarget();
            config.AddTarget("console", consoleTarget);

            var fileTarget = new FileTarget();
            config.AddTarget("file", fileTarget);

            var elasticsearchTarget = new ElasticsearchTarget();
            config.AddTarget("logstash", elasticsearchTarget);

            // Step 3. Set target properties 

            //const string layout = @"${date:format=HH\:MM\:ss} ${logger} ${message}";
            const string pattern = @"${message}";

            //consoleTarget.Layout = consoleTarget.Layout = layout;;

            //fileTarget.FileName = "${basedir}/${processname}.log";
            fileTarget.FileName = logFileName;
            fileTarget.Layout = new KibanaLayout(pattern,
                    new[] { "correlation_id", "context_id" });
            fileTarget.DeleteOldFileOnStartup = true;

            elasticsearchTarget.ElasticsearchUrl = TestSettings.ElasticsearchUrl;
            elasticsearchTarget.Layout = new KibanaLayout(pattern,
                    new[] { "correlation_id", "context_id" });

            // Step 4. Define rules
            var rule1 = new LoggingRule("*", LogLevel.Debug, consoleTarget);
            config.LoggingRules.Add(rule1);

            var rule2 = new LoggingRule("*", LogLevel.Debug, fileTarget);
            config.LoggingRules.Add(rule2);

            var rule3 = new LoggingRule("*", LogLevel.Debug, elasticsearchTarget);
            config.LoggingRules.Add(rule3);

            // Step 5. Activate the configuration
            LogManager.Configuration = config;
        }
    }
}
