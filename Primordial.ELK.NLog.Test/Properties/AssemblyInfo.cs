using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Primordial.ELK.NLog.Test")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Hyrtwol")]
[assembly: AssemblyProduct("Primordial.ELK.NLog.Test")]
[assembly: AssemblyCopyright("Copyright Hyrtwol 2015")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]
[assembly: Guid("e8397a46-94b8-4105-a65f-acac0fb3f5f9")]

[assembly: AssemblyVersion("1.0.1")]
[assembly: AssemblyFileVersion("1.0.1")]
