﻿using System;
using NLog.Config;
using NLog.Targets;
using NUnit.Framework;
using NL = NLog;

namespace Primordial.ELK.NLog.Test
{
    [TestFixture]
    public class KibanaLayoutTests
    {
        private static void ConfigureLogging()
        {
            var config = new LoggingConfiguration();

            var consoleTarget = new ConsoleTarget
            {
                Layout = new KibanaLayout(
                    @"${logger} ${gdc:item=correlation_id} ${mdc:item=context_id} [${level}]- ${message}",
                    "correlation_id", "context_id")
            };
            config.AddTarget("logstash", consoleTarget);

            var rule3 = new LoggingRule("*", NL.LogLevel.Debug, consoleTarget);
            config.LoggingRules.Add(rule3);

            NL.LogManager.Configuration = config;
        }

        [Test]
        public void LogWarning()
        {
            ConfigureLogging();

            var logger = NL.LogManager.GetLogger("Test");
            Assert.IsNotNull(logger);

            NL.GlobalDiagnosticsContext.Set("correlation_id", Guid.NewGuid().ToString("N"));
            NL.MappedDiagnosticsContext.Set("context_id", (1337000L).ToString());
            logger.Warn("Just testing the logger with a warning.");
        }

        [Test]
        public void LogWarningUsingLogEventInfo()
        {
            ConfigureLogging();

            var logger = NL.LogManager.GetLogger("Test");
            Assert.IsNotNull(logger);

            int i = 0;
            //for (int i = 0; i < 3; i++)
            {
                var theEvent = new NL.LogEventInfo(NL.LogLevel.Warn, "log-name", 
                    "Just testing the logger with a warning.");
                theEvent.Properties["context_id"] = (1337000L + i);
                logger.Log(theEvent);
            }
        }

        [Test]
        public void LogError()
        {
            ConfigureLogging();

            //log4net.ThreadContext.Properties["ContextId"] = 12345678L;

            var logger = NL.LogManager.GetLogger("Test");
            Assert.IsNotNull(logger);

            try
            {
                throw new NotSupportedException("Not a real exception!");
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Log4Net fake error " + DateTime.Now.Ticks);
            }
        }
    }
}