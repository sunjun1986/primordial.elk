using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using Newtonsoft.Json.Linq;

namespace Primordial.ELK
{
    public static class Globals
    {
        public const int DefaultElasticsearchPort = 9200;
        public const int DefaultRedisPort = 6379;

        public static readonly string OsVersion = Environment.OSVersion.ToString();

        public static readonly string ApplicationName = Assembly.GetExecutingAssembly()
            .GetName().Name.Replace('.','-').ToLowerInvariant();

        public static readonly JArray HostIps = new JArray(Dns.GetHostEntry(Dns.GetHostName()).AddressList
            .Where(ip => ip.AddressFamily == AddressFamily.InterNetwork).Select(ip => (object)ip.ToString())
            .ToArray());
    }
}