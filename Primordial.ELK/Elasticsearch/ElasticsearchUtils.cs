using System;
using System.Diagnostics;
using System.IO;
using System.Net;

namespace Primordial.ELK.Elasticsearch
{
    //public class ElasticsearchUrl
    //{
    //    public readonly Uri BaseUrl;

    //    public ElasticsearchUrl(Uri elasticsearchUrl)
    //    {
    //        BaseUrl = elasticsearchUrl;
    //    }

    //    public ElasticsearchUrl(string elasticsearchUrl)
    //        : this(new Uri(elasticsearchUrl))
    //    {
    //    }

    //    public Uri CreateIndexUri(string index, string type, string id)
    //    {
    //        return new Uri(BaseUrl, Path.Combine(index, type, id));
    //    }
    //}

    public static class ElasticsearchUtils
    {
        public static void SendToElasticsearch(Uri elasticsearchUrl, string json)
        {
            Send(CreateElasticsearcIndexUrl(elasticsearchUrl), json);
        }

        public static Uri CreateElasticsearcIndexUrl(Uri elasticsearchUrl)
        {
            var index = string.Format("logstash-{0:yyyy.MM.dd}", DateTime.UtcNow);
            var type = Globals.ApplicationName;
            var id = Guid.NewGuid().ToString("N");
            return CreateElasticsearcIndexUrl(elasticsearchUrl, index, type, id);
        }

        public static Uri CreateElasticsearcIndexUrl(Uri elasticsearchUrl, string index, string type, string id)
        {
            return new Uri(elasticsearchUrl, Path.Combine(index, type, id));
        }

        private static void Send(Uri logstashIndexUrl, string json)
        {
            Debug.Print("logstashIndexUrl={0}", logstashIndexUrl);
            //Debug.WriteLine("json=" + json);
            //using (var client = new WebClient())
            //{
            //    client.UploadData(logstashIndexUrl, Encoding.UTF8.GetBytes(json));
            //}

            var webRequest = WebRequest.Create(logstashIndexUrl);
            webRequest.Method = "POST";
            webRequest.Timeout = 5000; // in ms so 5 seconds
            webRequest.ContentType = "application/json";

            using (var s = webRequest.GetRequestStream())
            using (var sw = new StreamWriter(s))
                sw.Write(json);

            using (var response = (HttpWebResponse) webRequest.GetResponse())
            {
                if (response.StatusCode != HttpStatusCode.Created) throw new Exception("log entry was not created successfully in elasticsearch");
            }
        }
    }
}