﻿using Newtonsoft.Json.Serialization;

namespace Primordial.ELK.Kibana
{
    public class KibanaPropertyNamesContractResolver : DefaultContractResolver
    {
        protected override string ResolvePropertyName(string propertyName)
        {
            return ToKibanaField(propertyName);
        }

        public static string ToKibanaField(string s)
        {
            return string.IsNullOrEmpty(s) ? null : s.ToLowerInvariant();
        }
    }

    //public class ExceptionConverter : JsonConverter
    //{
    //    public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
    //    {
    //        if (value == null)
    //        {
    //            writer.WriteNull();
    //        }
    //        else
    //        {
    //            if (!(value is Exception))
    //                throw new JsonSerializationException("Expected Exception object value");
    //            writer.WriteValue(value.ToString());
    //        }
    //    }

    //    public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
    //    {
    //        if (reader.TokenType == JsonToken.Null)
    //            return (object)null;
    //        if (reader.TokenType != JsonToken.String)
    //            throw new JsonSerializationException(string.Format("Unexpected token or value when parsing version. Token: {0}, Value: {1}", (IFormatProvider)CultureInfo.InvariantCulture, (object)reader.TokenType, reader.Value));
    //        try
    //        {
    //            return (object)new Exception((string)reader.Value);
    //        }
    //        catch (Exception ex)
    //        {
    //            throw new JsonSerializationException(string.Format("Error parsing version string: {0}", (IFormatProvider)CultureInfo.InvariantCulture, reader.Value), ex);
    //        }
    //    }

    //    public override bool CanConvert(Type objectType)
    //    {
    //        return objectType == typeof(Exception);
    //    }
    //}

}