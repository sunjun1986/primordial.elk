using Newtonsoft.Json;

namespace Primordial.ELK.Kibana
{
    public static class KibanaUtils
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            ContractResolver = new KibanaPropertyNamesContractResolver(),
            Formatting = Formatting.Indented
        };

        public static string ToJson(this LogEvent logEvent)
        {
            return JsonConvert.SerializeObject(logEvent, Settings);
        }

#if NOGO
        public static string ToKibanaField(this string s)
        {
            if (string.IsNullOrEmpty(s)) return null;

            s = s.ToLowerInvariant();
            char[] chArray = s.ToCharArray();
            //char[] chArray = s.ToUpperInvariant().ToCharArray();
            //for (int index = 0; index < chArray.Length; ++index)
            //{
            //    bool flag = index + 1 < chArray.Length;
            //    if (index <= 0 || !flag || char.IsUpper(chArray[index + 1]))
            //        chArray[index] = char.ToLower(chArray[index], CultureInfo.InvariantCulture);
            //    else
            //        break;
            //}
            var cnt = chArray.Length;
            for (int i = 0; i < cnt; i++)
            {
                var ch = chArray[i];
                if (ch == '_')
                {
                    chArray[i] = '-';
                }
                //chArray[i] = 'X';
            }
            return new string(chArray);
            return s;
        }
#endif
    }
}