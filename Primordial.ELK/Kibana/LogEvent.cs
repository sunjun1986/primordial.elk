using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using Newtonsoft.Json;
// ReSharper disable UnusedMember.Global
// ReSharper disable InconsistentNaming
// ReSharper disable NotAccessedField.Global
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable CollectionNeverQueried.Global

namespace Primordial.ELK.Kibana
{
    public class LogEvent
    {
        //private static readonly object[] EmptyList = {};
        //private static readonly string[] EmptyTags = {};

        [JsonProperty(PropertyName = "@timestamp")]
        //[JsonConverter(typeof(JavaScriptDateTimeConverter))]
        public DateTime timestamp;

        //[JsonProperty(PropertyName = "@host")]
        public Host host = Host.Create();

        //[JsonProperty(PropertyName = "@application")]
        public Application application = Application.Create();

        //[JsonProperty(PropertyName = "@logger")]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public Logger log;

        //[JsonProperty(PropertyName = "@tags")]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public HashSet<string> tags;

        //[JsonProperty(PropertyName = "@exceptions")]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        //[JsonConverter(typeof(Newtonsoft.Json.Converters.VersionConverter))]
        public object exception;

        //[JsonProperty(PropertyName = "@fields")]
        //public object fields = new object();
        public readonly Dictionary<string, object> fields = new Dictionary<string, object>();

        //[JsonProperty(PropertyName = "@message")]
        public string message;

        public void AddException(Exception ex)
        {
            if (ex == null) return;
            exception =
                new
                {
                    message = ex.Message,
                    stacktrace = ex.StackTrace
                };
        }

        public void AddField(string key, object value)
        {
            fields.Add(key, value);
        }

        public void AddTags(params string[] tag)
        {
            if(tags == null) tags = new HashSet<string>();
            foreach (var t in tag)
            {
                tags.Add(t);
            }
        }

        public void AddLogInfo(string level, string name, string type)
        {
            log = new Logger
            {
                level = level,
                name = name,
                type = type
            };
        }

        public void AddStackFrame(StackFrame stackFrame)
        {
            if (stackFrame == null) return;
            var method = stackFrame.GetMethod();
            AddField("stackframe",
                new
                {
                    path = stackFrame.GetFileName(),
                    line = stackFrame.GetFileLineNumber(),
                    method = method.Name,
                    @class = method.DeclaringType?.ToString()
                });
        }

    }

    public class Host
    {
        public string name;
        public string user;
        public string os;
        public int cpu;
        public bool x64;
        //public int pagesize;
        //public string[] ips;
        public string ips;

        public static Host Create()
        {
            var host = new Host();
            host.name = Environment.MachineName;
            host.user = Environment.UserName;
            host.os = Environment.OSVersion.ToString();
            host.x64 = Environment.Is64BitOperatingSystem;
            //host.bits = Environment.Is64BitOperatingSystem ? 64 : 32;
            host.cpu = Environment.ProcessorCount;
            host.ips = string.Join(";", GetIps());
            return host;
        }

        private static IEnumerable<string> GetIps()
        {
            return Dns.GetHostEntry(Dns.GetHostName()).AddressList
                .Where(ip => ip.AddressFamily == AddressFamily.InterNetwork)
                .Select(ip => ip.ToString());
        }
    }

    public class Application
    {
        public string name;
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string version;
        public string domain;
        public long gcmem;
        //public string codebase;

        public static Application Create()
        {
            var domain = AppDomain.CurrentDomain;
            var setupInformation = domain.SetupInformation;
            //var domainManager = domain.DomainManager;
            var app = new Application();
            //app.basepath = domain.BaseDirectory;
            app.domain = domain.FriendlyName;
            app.name = setupInformation.ApplicationName;
            var asm = Assembly.GetEntryAssembly();
            //var asm = domainManager.EntryAssembly;
            if (asm != null)
            {
                var an = asm.GetName();
                //var unc = an.CodeBase.ToLowerInvariant();
                //var uri = new UriBuilder(unc);
                //uri.Host = Environment.MachineName;
                //app.codebase = uri.Uri.AbsoluteUri;
                //app.codebase = an.CodeBase.ToLowerInvariant();
                app.version = an.Version.ToString();
            }
            app.gcmem = GC.GetTotalMemory(false) >> 10;
            return app;
        }
    }

    public class Logger
    {
        public string level;
        public string name;
        public string type;
    }
}