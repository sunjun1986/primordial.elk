using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Primordial.ELK")]
[assembly: AssemblyDescription("ElasticSearch, Logstash and Kibana")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Hyrtwol")]
[assembly: AssemblyProduct("Primordial.ELK")]
[assembly: AssemblyCopyright("Copyright Hyrtwol 2015")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]
[assembly: Guid("3b3241b7-a60f-45a7-9629-2f5f53d3ccd3")]

[assembly: AssemblyVersion("1.0.1")]
[assembly: AssemblyFileVersion("1.0.1")]
