using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Primordial.ELK.MSBuild.Test")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Hyrtwol")]
[assembly: AssemblyProduct("Primordial.ELK.MSBuild.Test")]
[assembly: AssemblyCopyright("Copyright Hyrtwol 2015")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]
[assembly: Guid("6dd02a24-e49c-4bfe-8520-2f436306c7b2")]

[assembly: AssemblyVersion("1.0.1")]
[assembly: AssemblyFileVersion("1.0.1")]
