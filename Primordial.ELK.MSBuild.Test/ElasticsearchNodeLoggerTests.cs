﻿#if REDIS
using System;
using Microsoft.Build.BuildEngine;
using Microsoft.Build.Framework;
using NUnit.Framework;

namespace Primordial.ELK.MSBuild.Test
{
#if !IGNORE_UNIT_TEST
    [Ignore("TeamCity is unable to connect to redis")]
#endif
    [TestFixture]
    public class ElasticsearchNodeLoggerTests
    {
        [Test]
        public void LogBuild()
        {
#pragma warning disable 612, 618
            var engine = new Engine();
#pragma warning restore 612, 618

            try
            {
                ILogger logger = new LogstashNodeLogger();
                logger.Parameters = "host=redis.dev.247ms.com;port=6379";
                engine.RegisterLogger(logger);

                var project = engine.CreateNewProject();

                project.DefaultTargets = "Build";

                Assert.IsNotNull(project.Targets);
                var buildTarget = project.Targets.AddNewTarget("Build");

                var task = buildTarget.AddNewTask("Message");
                task.SetParameterValue("Text", "MSBuild fake error " + DateTime.Now.Ticks);
                task.SetParameterValue("Importance", "high");

                Assert.IsTrue(project.Build());
            }
            finally
            {
                engine.UnloadAllProjects();
                engine.UnregisterAllLoggers();
                engine.Shutdown();
            }
        }
    }
}
#endif