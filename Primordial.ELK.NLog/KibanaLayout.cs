﻿using System.Linq;
using NLog;
using NLog.Layouts;
using Primordial.ELK.Kibana;

namespace Primordial.ELK.NLog
{
    [Layout("Kibana")]
    public class KibanaLayout : SimpleLayout
    {
        private readonly string[] _paramsToMap;

        public KibanaLayout()
            : this("%message%newline")
        {
        }

        public KibanaLayout(string pattern)
            : base(pattern)
        {
            _paramsToMap = new[] { "correlation_id", "context_id" };
        }

        public KibanaLayout(string pattern, params string[] paramsToMap)
            : base(pattern)
        {
            _paramsToMap = paramsToMap;
        }
        
        protected override string GetFormattedMessage(LogEventInfo loggingEvent)
        {
            var messageEvent = new LogEvent();

            messageEvent.timestamp = loggingEvent.TimeStamp;
            messageEvent.AddLogInfo(loggingEvent.Level.ToString(), loggingEvent.LoggerName, "NLog");

            messageEvent.AddException(loggingEvent.Exception);
            messageEvent.AddStackFrame(loggingEvent.UserStackFrame);

            //fields.Add("configFile", appDomain.SetupInformation.ConfigurationFile);
            foreach (var paramName in _paramsToMap.Where(GlobalDiagnosticsContext.Contains))
            {
                //Debug.Print("global.property={0}", paramName);
                messageEvent.AddField(paramName, GlobalDiagnosticsContext.Get(paramName));
            }
            foreach (var paramName in _paramsToMap.Where(MappedDiagnosticsContext.Contains))
            {
                //Debug.Print("mapped.property={0}", paramName);
                messageEvent.AddField(paramName, MappedDiagnosticsContext.Get(paramName));
            }
            foreach (var property in loggingEvent.Properties)
            {
                var key = property.Key.ToString();
                //Debug.Print("loggingEvent.property={0}", key);
                messageEvent.AddField(key, property.Value.ToString());
            }

            messageEvent.message = base.GetFormattedMessage(loggingEvent);

            var json = messageEvent.ToJson();
            return json;
        }

#if NOGO
        protected override string GetFormattedMessage(LogEventInfo loggingEvent)
        {
            var stackFrame = loggingEvent.UserStackFrame;
            var jEvent = new JObject();

            //jEvent["@source"] = loggingEvent.LoggerName;
            //jEvent["@source_host"] = Environment.MachineName;
            //jEvent["@timestamp"] = loggingEvent.TimeStamp.ToString("o");
            ////jEvent["@type"] = Globals.LogstashType; // ApplicationName;
            jEvent.AddMantory(loggingEvent.LoggerName, loggingEvent.TimeStamp);
            jEvent.AddLogInfo(loggingEvent.Level.ToString(), loggingEvent.LoggerName, "NLog");
            //jEvent.Add("@tags", new JArray
            //{
            //    "E247.Logstash",
            //    "NLog",
            //    loggingEvent.Level.ToString()
            //});

            //jEvent.AddTags(loggingEvent.Level.ToString());

            //            fields.Add("application", new JObject
            //            {
            //#if FORMSAPP
            //                {"name", Application.ProductName},
            //                {"version", Application.ProductVersion},
            //                {"executable", Application.ExecutablePath},
            //#endif
            //                {"basepath", AppDomain.CurrentDomain.BaseDirectory},
            //                {"domain", AppDomain.CurrentDomain.FriendlyName}
            //            });
            //fields.Add("host", new JObject
            //{
            //    {"os_version", Globals.OsVersion},
            //    {"ips", Globals.HostIps}
            //});
            //fields.Add("log", new JObject
            //{
            //    {"level", loggingEvent.Level.ToString()},
            //    {"name", loggingEvent.LoggerName}
            //});
            //fields.Add("threadName",  loggingEvent.ThreadName );

            var ex = loggingEvent.Exception;
            if (ex != null)
            {
                jEvent.AddException(ex);
            }

            var fields = new JObject();
            if (stackFrame != null) fields.AddStackFrame(stackFrame);

            //var current = WindowsIdentity.GetCurrent();
            //if (current != null)
            //{
            //    fields.Add("user_name", current.Name);
            //}

            //fields.Add("configFile", appDomain.SetupInformation.ConfigurationFile);


            foreach (var paramName in _paramsToMap.Where(GlobalDiagnosticsContext.Contains))
            {
                //Debug.Print("global.property={0}", paramName);
                fields.Add(paramName, GlobalDiagnosticsContext.Get(paramName));
            }
            foreach (var paramName in _paramsToMap.Where(MappedDiagnosticsContext.Contains))
            {
                //Debug.Print("mapped.property={0}", paramName);
                fields.Add(paramName, MappedDiagnosticsContext.Get(paramName));
            }

            foreach (var property in loggingEvent.Properties)
            {
                var key = property.Key.ToString();
                Debug.Print("loggingEvent.property.Key={0}", key);
                fields.Add(key, property.Value.ToString());
            }

            jEvent.AddFields(fields);

            jEvent.AddMessage(base.GetFormattedMessage(loggingEvent));

            var settings = new JsonSerializerSettings();
            settings.Formatting = Formatting.Indented;
            //settings.ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver();
            settings.ContractResolver = new KibanaPropertyNamesContractResolver();

            //var json = JsonConvert.SerializeObject(jEvent, Formatting.Indented);
            var json = JsonConvert.SerializeObject(jEvent, settings);
            //Debug.WriteLine("json-layout=" + json);
            return json;
        }
#endif
    }
}