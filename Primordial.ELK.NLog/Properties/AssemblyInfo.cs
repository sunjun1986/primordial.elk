using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Primordial.ELK.NLog")]
[assembly: AssemblyDescription("ELK for NLog 4.1.2")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Hyrtwol")]
[assembly: AssemblyProduct("Primordial.ELK.NLog")]
[assembly: AssemblyCopyright("Copyright Hyrtwol 2015")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]
[assembly: Guid("27911836-0f44-406a-88c5-c89b232a107a")]

[assembly: AssemblyVersion("1.0.1")]
[assembly: AssemblyFileVersion("1.0.1")]
