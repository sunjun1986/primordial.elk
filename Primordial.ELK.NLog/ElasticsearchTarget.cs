﻿using System;
using System.Diagnostics;
using NLog;
using NLog.Config;
using NLog.Targets;
using Primordial.ELK.Elasticsearch;

namespace Primordial.ELK.NLog
{
    [Target("Elasticsearch")]
    public sealed class ElasticsearchTarget : TargetWithLayout
    {
        [RequiredParameter]
        public string ElasticsearchUrl { get; set; }

        public string Type { get; set; }

        public ElasticsearchTarget()
        {
            Type = typeof(ElasticsearchTarget).Name;
        }

        protected override void Dispose(bool disposing)
        {
            Debug.WriteLine("ElasticsearchTarget.Dispose()");
            base.Dispose(disposing);
        }

        protected override void Write(LogEventInfo logEvent)
        {
            if (logEvent == null) throw new ArgumentNullException(nameof(logEvent));
            var json = Layout.Render(logEvent);

            var elasticsearchUrl = new Uri(ElasticsearchUrl);
            ElasticsearchUtils.SendToElasticsearch(elasticsearchUrl, json);
        }
    }
}
