﻿using System;
using System.Diagnostics;
using log4net;
using log4net.Core;
using log4net.Repository.Hierarchy;
using NUnit.Framework;
using Primordial.ELK.Test;

namespace Primordial.ELK.log4net.Test
{
#if IGNORE_UNIT_TEST
    [Ignore("TeamCity is unable to connect to the remove host")]
#endif
    [TestFixture]
    public class ElasticsearchAppenderTests
    {
        private static void InitHierarchy()
        {
            var hierarchy = LogManager.GetRepository() as Hierarchy;
            Assert.IsNotNull(hierarchy);
            hierarchy.Root.Level = Level.All;

            //var consoleAppender = new ConsoleAppender();
            //consoleAppender.Layout = new LogstashLayout(@"%logger (%property{context_id}) [%level] %message");
            //consoleAppender.ActivateOptions();
            //hierarchy.Root.AddAppender(consoleAppender);

            var elasticsearchAppender = new ElasticsearchAppender();
            elasticsearchAppender.Layout = new KibanaLayout(@"%logger (%property{context_id}) [%level] %message");
            elasticsearchAppender.ElasticsearchUrl = TestSettings.ElasticsearchUrl;
            elasticsearchAppender.ActivateOptions();
            hierarchy.Root.AddAppender(elasticsearchAppender);
            
            hierarchy.Configured = true; //mark repository as configured
            hierarchy.RaiseConfigurationChanged(EventArgs.Empty); //notify that is has changed.
        }

        [Test]
        public void LogWarning()
        {
            InitHierarchy();

            ILog logger = LogManager.GetLogger("ElasticsearchAppenderTests.LogWarning");
            Assert.IsNotNull(logger);

            for (int i = 0; i < 3; i++)
            {
                Debug.WriteLine("Logging warning: {0}", i);
                logger.WarnFormat("Warning {0}", i);
            }
        }

        [Test]
        public void LogError()
        {
            InitHierarchy();

            ILog logger = LogManager.GetLogger("ElasticsearchAppenderTests.LogError");
            Assert.IsNotNull(logger);

            for (int i = 0; i < 3; i++)
            {
                //Debug.WriteLine("Logging warning: {0}", i);
                //logger.ErrorFormat("Fake Error {0}", i);
                //logger.Error("Fake Error " + i, new Excep);

                try
                {
                    throw new NotSupportedException("Not a real exception! "+i);
                }
                catch (Exception ex)
                {
                    //Debug.WriteLine("Logging error");
                    logger.Error("Fake error " +  i, ex);
                }
            }
        }

        [Test]
        public void LogWarningWithLogEvent()
        {
            InitHierarchy();

            ILog logger = LogManager.GetLogger("ElasticsearchAppenderTests.LogWarningWithLogEvent");
            Assert.IsNotNull(logger);

            for (int i = 0; i < 3; i++)
            {
                Debug.WriteLine("Logging warning: {0}", i);
                var data = new LoggingEventData();
                data.Message = "Just a test";
                var loggingEvent = new LoggingEvent(data);
                logger.Logger.Log(loggingEvent);
                //logger.WarnFormat("Warning {0}", i);
            }
        }

        [Test]
        public void LogWarningWithThreadProperties()
        {
            InitHierarchy();

            // http://www.beefycode.com/post/Log4Net-Tutorial-pt-6-Log-Event-Context.aspx
            //log4net.GlobalContext.Properties["CustomColumn"] = "Custom value";
            ThreadContext.Properties["batch_id"] = 666;

            ILog logger = LogManager.GetLogger("ElasticsearchAppenderTests.LogWarningWithThreadProperties");
            Assert.IsNotNull(logger);
            //logger.Logger.Repository.Properties

            for (int i = 0; i < 3; i++)
            {
                Debug.WriteLine("Logging warning: {0}", i);
                ThreadContext.Properties["context_id"] = 1337000L + i;
                logger.WarnFormat("Warning {0}", i);
            }
        }

        [Test]
        public void LogErrorWithThreadProperties()
        {
            InitHierarchy();

            // http://www.beefycode.com/post/Log4Net-Tutorial-pt-6-Log-Event-Context.aspx
            ThreadContext.Properties["context_id"] = 12345678L;

            ILog logger = LogManager.GetLogger("ElasticsearchAppenderTests.LogErrorWithThreadProperties");
            Assert.IsNotNull(logger);

            try
            {
                throw new NotSupportedException("Not a real exception!");
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Logging error");
                logger.Error("Log4Net fake error " + DateTime.Now.Ticks, ex);
            }
        }
    }
}
