﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using log4net;
using log4net.Appender;
using log4net.Core;
using log4net.Repository.Hierarchy;
using log4net.Util;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NUnit.Framework;

namespace Primordial.ELK.log4net.Test
{
    [TestFixture]
    public class KibanaLayoutLayoutTests
    {
        [Test]
        public void RenderOutput()
        {
            var layout = new KibanaLayout(@"%logger (%property{context_id}) [%level] :: %message");
            var timestamp = DateTime.UtcNow;
            var data = new LoggingEventData
                {
                    Message = "Just a test",
                    Level = Level.Info,
                    LoggerName = "-logger-name-",
                    Identity = "-identity-",
                    LocationInfo = new LocationInfo("-class-", "-method-", "-path-", "666"),
                    ThreadName = "-thread-name-",
                    TimeStamp = timestamp,
                    UserName = "-user-",
                    Domain = "-domain-"
                };
            var properties = new PropertiesDictionary();
            properties["context_id"] = 123456L;
            data.Properties = properties;

            var loggingEvent = new LoggingEvent(data);

            var sb = new StringBuilder();
            using (var writer = new StringWriter(sb))
            {
                layout.Format(writer, loggingEvent);
            }
            Debug.WriteLine(sb);
            var jObject = (JObject)JsonConvert.DeserializeObject(sb.ToString());

            Assert.AreEqual("-logger-name- (123456) [INFO] :: Just a test", (string)jObject["message"]);
            Assert.AreEqual(timestamp, (DateTime)jObject["@timestamp"]);
            Assert.AreEqual(Environment.MachineName, (string)jObject["host"]["name"].ToString());

            Assert.AreEqual("INFO", jObject["log"]["level"].ToString());
            Assert.AreEqual("-logger-name-", jObject["log"]["name"].ToString());

            //Assert.AreEqual("-path-", fields["location"]["path"].ToString());
            //Assert.AreEqual("666", fields["location"]["line"].ToString());
            //Assert.AreEqual("-method-", fields["location"]["method"].ToString());
            //Assert.AreEqual("-class-", fields["location"]["class"].ToString());

            //Assert.AreEqual("-domain-", jObject["application"]["domain"].ToString());

            //Assert.AreEqual("NUnit Primordial.ELK.log4net.Test", jObject["application"]["domain"].ToString());

            var fields = jObject["fields"];
            Assert.IsNotNull(fields);

            //Assert.AreEqual("-identity-", fields["identity"].ToString());
            //Assert.AreEqual("-user-", fields["user_name"].ToString());
            Assert.AreEqual("123456", fields["context_id"].ToString());

        }

        private static void ConfigureLogging()
        {
            var hierarchy = LogManager.GetRepository() as Hierarchy;
            Assert.IsNotNull(hierarchy);
            hierarchy.Root.Level = Level.All;

            var consoleAppender = new ConsoleAppender();
            consoleAppender.Layout = new KibanaLayout(@"%logger (%property{ContextId}) [%level]- %message");
            consoleAppender.ActivateOptions();
            hierarchy.Root.AddAppender(consoleAppender);

            //mark repository as configured and  
            //notify that is has changed.  
            hierarchy.Configured = true;
            hierarchy.RaiseConfigurationChanged(EventArgs.Empty);
        }

        [Test]
        public void LogWarning()
        {
            ConfigureLogging();

            var logger = LogManager.GetLogger("Test");
            Assert.IsNotNull(logger);

            ThreadContext.Properties["context_id"] = 1337000L;
            logger.WarnFormat("Just testing the logger with a warning.");
        }

        [Test]
        public void LogError()
        {
            ConfigureLogging();

            ThreadContext.Properties["contextId"] = 12345678L;

            ILog logger = LogManager.GetLogger("Test");
            Assert.IsNotNull(logger);

            try
            {
                throw new NotSupportedException("Not a real exception!");
            }
            catch (Exception ex)
            {
                logger.Error("Log4Net fake error " + DateTime.Now.Ticks, ex);
            }
        }
    }
}