﻿#define USE_LOGSTASH
using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using log4net.Appender;
using log4net.Core;
using log4net.Filter;
using log4net.Layout;
using log4net.Repository.Hierarchy;

namespace Primordial.ELK.log4net.Test
{
    public static class LogUtils
    {
        public static readonly bool IsDebug =
#if DEBUG
            true;
#else
            false;
#endif

        public static string LogFileName = "rolling.log";
        public static string DebugLogFileName = "debug.log";

        //public static void SetupLog4Net()
        //{
        //    SetupLog4Net(Environment.UserInteractive);
        //}

        //public static void SetupLog4Net(bool consoleAppender, string eventSource = null)
        //{
        //    try
        //    {
        //        //Get the logger repository hierarchy.  
        //        var hierarchy = LogManager.GetRepository() as Hierarchy;
        //        if (hierarchy == null)
        //            throw new NullReferenceException("Unable to cast LogManager.GetRepository() to " +
        //                                             typeof(Hierarchy));

        //        if (IsDebug) AddDebugFileAppender(hierarchy);
        //        else AddRollingFileAppender(hierarchy);

        //        if (consoleAppender) AddColoredConsoleAppender(hierarchy);

        //        if (!string.IsNullOrEmpty(eventSource))
        //        {
        //            //AddEventLogAppender(hierarchy, "TexlLog", AppDomain.CurrentDomain.FriendlyName);
        //            AddEventLogAppender(hierarchy, eventSource, "Application");
        //        }

        //        AddLogstashAppender(hierarchy);

        //        //var environment = ConfigurationManager.AppSettings["environment"];
        //        //if (!string.IsNullOrEmpty(environment))
        //        //{
        //        //    switch (environment)
        //        //    {
        //        //        case "Development":
        //        //            break;
        //        //        case "Test":
        //        //            break;
        //        //        case "Production":
        //        //            break;
        //        //    }
        //        //}

        //        //configure the logging at the root.  
        //        hierarchy.Root.Level = GetLogLevel(); //Level.All;

        //        //mark repository as configured and  
        //        //notify that is has changed.  
        //        hierarchy.Configured = true;
        //        hierarchy.RaiseConfigurationChanged(EventArgs.Empty);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception("SetupLog4Net Error: " + ex.Message, ex);
        //    }
        //}

        //[Conditional("USE_LOGSTASH")]
        //private static void AddLogstashAppender(Hierarchy hierarchy)
        //{
        //    string host = ConfigurationManager.AppSettings["logstashRedisHost"];
        //    if (string.IsNullOrEmpty(host)) return;

        //    Debug.Print("AddLogstashAppender host={0}", host);
        //    try
        //    {
        //        var fileAppender = new RedisAppender();
        //        fileAppender.Layout = new LogstashLayout("%-6p %m%n");
        //        fileAppender.RedisHost = host;

        //        fileAppender.AddFilter(new LevelRangeFilter { LevelMin = Level.Info });

        //        fileAppender.ActivateOptions();
        //        hierarchy.Root.AddAppender(fileAppender);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception("AddRedisAppender Error: " + ex.Message, ex);
        //    }
        //}

        const string FILE_APPENDER_PATTERN = "%date{HH:mm:ss,fff} %4t %-6p %m%n";

        private static void AddDebugFileAppender(Hierarchy hierarchy)
        {
            try
            {
                var fileAppender = new FileAppender();
                fileAppender.Layout = new PatternLayout(FILE_APPENDER_PATTERN);
                fileAppender.AppendToFile = false;
                fileAppender.File = GetLogFileName(DebugLogFileName);

                //fileAppender.AddFilter(new LevelRangeFilter() { LevelMin = Level.Warn });

                fileAppender.ActivateOptions();
                hierarchy.Root.AddAppender(fileAppender);
            }
            catch (Exception ex)
            {
                throw new Exception("AddDebugFileAppender Error: " + ex.Message, ex);
            }
        }

        private static void AddRollingFileAppender(Hierarchy hierarchy)
        {
            try
            {
                var fileAppender = new RollingFileAppender();
                fileAppender.Layout = new PatternLayout(FILE_APPENDER_PATTERN);
                fileAppender.AppendToFile = true;
                fileAppender.File = GetLogFileName(LogFileName);
                fileAppender.MaxFileSize = 50 * 1024 * 1024;
                fileAppender.CountDirection = 1;
                fileAppender.MaxSizeRollBackups = 30;
                fileAppender.RollingStyle = RollingFileAppender.RollingMode.Date;
                fileAppender.ActivateOptions();
                hierarchy.Root.AddAppender(fileAppender);
            }
            catch (Exception ex)
            {
                throw new Exception("AddRollingFileAppender Error:" + ex.Message, ex);
            }
        }

        [Conditional("USE_EVENTLOG")]
        private static void AddEventLogAppender(Hierarchy hierarchy, string eventSource, string logName)
        {
            Debug.Print("AddEventLogAppender eventSource={0}, logName={1}", eventSource, logName);
            try
            {
                //Console.WriteLine("Creating EventLogAppender eventSource={0} logName={1}", eventSource, logName);

                //var eventSource = "E247.Api.User";
                //var eventSource = ConfigurationManager.AppSettings["logPath"];

                //if (!EventLog.SourceExists(eventSource))
                //{
                //    EventLog.CreateEventSource(eventSource, logName);
                //}

                //EventLog.DeleteEventSource();

                var eventLogAppender = new EventLogAppender();
                eventLogAppender.Layout = new PatternLayout(@"%d %p %c - %m%n");
                eventLogAppender.LogName = logName;
                //eventLogAppender.ApplicationName = AppDomain.CurrentDomain.FriendlyName;
                eventLogAppender.ApplicationName = eventSource;
                eventLogAppender.AddFilter(new LevelRangeFilter { LevelMin = Level.Error, LevelMax = Level.Fatal });
                eventLogAppender.ActivateOptions();
                hierarchy.Root.AddAppender(eventLogAppender);
            }
            catch (Exception ex)
            {
                throw new Exception("AddEventLogAppender Error:" + ex.Message, ex);
            }
        }

        private static void AddColoredConsoleAppender(Hierarchy hierarchy)
        {
            try
            {
                var consoleAppender = new ColoredConsoleAppender();
                consoleAppender.Layout = new PatternLayout(@"%m%n");
                consoleAppender.AddMapping(new ColoredConsoleAppender.LevelColors { Level = Level.Debug, ForeColor = ColoredConsoleAppender.Colors.White | ColoredConsoleAppender.Colors.HighIntensity });
                consoleAppender.AddMapping(new ColoredConsoleAppender.LevelColors { Level = Level.Info, ForeColor = ColoredConsoleAppender.Colors.Green | ColoredConsoleAppender.Colors.HighIntensity });
                consoleAppender.AddMapping(new ColoredConsoleAppender.LevelColors { Level = Level.Warn, ForeColor = ColoredConsoleAppender.Colors.Yellow | ColoredConsoleAppender.Colors.HighIntensity });
                consoleAppender.AddMapping(new ColoredConsoleAppender.LevelColors { Level = Level.Error, ForeColor = ColoredConsoleAppender.Colors.Red | ColoredConsoleAppender.Colors.HighIntensity });
                consoleAppender.AddMapping(new ColoredConsoleAppender.LevelColors { Level = Level.Fatal, ForeColor = ColoredConsoleAppender.Colors.Red | ColoredConsoleAppender.Colors.HighIntensity, BackColor = ColoredConsoleAppender.Colors.White });
                consoleAppender.ActivateOptions();
                hierarchy.Root.AddAppender(consoleAppender);
            }
            catch (Exception ex)
            {
                throw new Exception("AddEventLogAppender Error:" + ex.Message, ex);
            }
        }

        private static string GetLogFileName(string value)
        {
            try
            {
                var logPath = ConfigurationManager.AppSettings["logPath"];
                Debug.Print("logPath {0}", logPath);
                var file = string.IsNullOrEmpty(logPath) ? value : Path.Combine(logPath, value);
                Debug.Print("file {0}", file);
                var fileExpanded = Environment.ExpandEnvironmentVariables(file);
                Debug.Print("fileExpanded {0}", fileExpanded);
                return fileExpanded;
            }
            catch (Exception ex)
            {
                throw new Exception("GetLogFileName Error:" + ex.Message, ex);
            }
        }

#if NOGO
        private static Level GetLogLevel()
        {
            try
            {
                var logLevel = ConfigurationManager.AppSettings["logLevel"];
                if (logLevel != null)
                    switch (logLevel.ToLower())
                    {
                        case "debug":
                            return Level.Debug;
                        case "info":
                            return Level.Info;
                        case "warn":
                            return Level.Warn;
                        case "error":
                            return Level.Error;
                        case "fatal":
                            return Level.Fatal;
                    }
                return Level.All;
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.Message);
                return Level.All;
            }
        }

        public static void InitLogForOctopus()
        {
            var octopusPackageName = ConfigurationManager.AppSettings["OctopusPackageName"];
            if (string.IsNullOrWhiteSpace(octopusPackageName)) throw new InvalidOperationException("Missing OctopusPackageName");
            LogFileName = octopusPackageName + ".log";
            DebugLogFileName = octopusPackageName + ".debug.log";
        }
#endif
    }

    //private static readonly ILog Logger = LogManager.GetLogger(typeof(Application));

    //#region log4net hooks

    //private static void DebugLog(string message)
    //{
    //    Console.WriteLine("DEBUG: {0}", message);
    //}

    //private static void InfoLog(string message)
    //{
    //    Console.WriteLine("INFO: {0}", message);
    //}

    //private static void WarnLog(string message)
    //{
    //    Console.WriteLine("WARN: {0}", message);
    //}

    //private static void ErrorLog(string message)
    //{
    //    Console.WriteLine("ERROR: {0}", message);
    //}

    //private static void FatalLog(string message)
    //{
    //    Console.WriteLine("FATAL: {0}", message);
    //    Console.WriteLine("Stopping listeners");
    //    //StopAllListeners();
    //}

    //internal static void SetupDelegateAppender()
    //{
    //    DelegateAppender.SetupDelegates(Logger, DebugLog, InfoLog, WarnLog, ErrorLog, FatalLog);
    //}

    //#endregion

    //public class TexlFileAppender : FileAppender
    //{
    //    public override string File
    //    {
    //        get
    //        {
    //            return base.File;
    //        }
    //        set
    //        {
    //            base.File = LogUtils.GetLogFileName(value);
    //        }
    //    }
    //}

    //public class TexlRollingFileAppender : RollingFileAppender
    //{
    //    public override string File
    //    {
    //        get
    //        {
    //            return base.File;
    //        }
    //        set
    //        {
    //            base.File = LogUtils.GetLogFileName(value);
    //        }
    //    }
    //}
}